golang-github-rivo-tview (0.0~git20230530.8bd761d-1) unstable; urgency=medium

  * New upstream version 0.0~git20230530.8bd761d

 -- Daniel Milde <daniel@milde.cz>  Sun, 04 Feb 2024 22:24:27 +0100

golang-github-rivo-tview (0.0~git20221029.c4a7e50-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 0.0~git20221029.c4a7e50
  * Change Section from devel to golang
  * Bump debhelper build-dependency to debhelper-compat (= 13)"
  * Mark library package with "Multi-Arch: foreign"
  * Use dh-sequence-golang instead of dh-golang and --with=golang
  * Bump Standards-Version to 4.6.2 (no change)

 -- Anthony Fok <foka@debian.org>  Wed, 25 Jan 2023 16:18:56 -0700

golang-github-rivo-tview (0.0~git20220916.2e69b73-1) unstable; urgency=medium

  [ Richard Stanley ]
  * added libgen-tui

  [ 寻寻觅觅的Gopher ]
  * added kubectl-lazy

  [ Navid Yaghoobi ]
  * Adding podman-tui to the list of projects that use tview

  [ bagus andika ]
  * added domino-card

  [ Navid Yaghoobi ]
  * Adding tvxwidgets to the list of projects that use tview

  [ glendsoza ]
  * Added goaround to the list of projects using tview

  [ Abdfn ]
  * adding `abdfnx/resto` to the list of projects that use tview

  [ Navid Yaghoobi ]
  * update podman-tui link (https://github.com/containers/podman-tui) in
    list of projects that use tview - project migrated to containers repository

  [ Simon Paul ]
  * Add twad to list of applications that use tview

  [ Oliver ]
  * Added TextView.GetOriginalLineCount().
  * Added List methods to set styles in addition to colors. See #611
  * Added methods to modify the styles of the autocomplete drop-down. Resolves #671, resolves #672
  * Calling Table.Select() will also clamp the selection to the visible screen. Fixes #683
  * Added a note about TextView not being suitable for large texts.
  * Added RemoveOption() and GetOptionCount() to DropDown. Resolves #682
  * Fixed multiline text modals. Fixes #696
  * Updated documentation about intercepting keyboard events. Resolves #699
  * Bugfix: Movement on empty tables could lead to infinite loops. Fixes #705, resolves #706

  [ moson-mo ]
  * Add project "pacseek" to README

  [ Oliver ]
  * Minor clarification.
  * Added a funding button to the repo.
  * Simplified funding button.

  [ Tai ]
  * Add demo implementation of 7GUIs tasks

  [ Oliver ]
  * Added a link to the sponsoring page.
  * Some basic TextArea code.
  * Upgraded to new rivo/uniseg version.
  * Updated TextArea specification with more keyboard events.
  * Adapted to slight change in rivo/uniseg.

  [ Ashis Paul ]
  * Add project tuihub to readme

  [ Oliver ]
  * Implemented basic text area printing.
  * TextArea bugfixes. Also started definining the cursor.
  * Showing cursor.
  * Updated Go dependency to v1.18 due to rivo/uniseg. Fixes #750
  * Basic navigation.
  * Trying to wrap up basic navigation.
  * Minor uniseg upgrade.
  * Another minor uniseg upgrade.
  * rivo/uniseg went through some turbulences.
  * Finished navigation with word jumping.
  * Added basic text entering.
  * Added more editing functions.
  * Not promoting a mouse-up event to a click event when it was prevented by a mouse capture. See #752

  [ Aurelio Calegari ]
  * Update README.md
  * Update README.md

  [ Oliver ]
  * Implemented line deletion (Ctrl-U).
  * Implemented selections.
  * Implemented the clipboard.
  * Added tabs. Also added TextArea to main documentation.
  * Reimplemented TextArea.replace() function, considering undo handling now.
  * Implemented undo/redo.
  * Added mouse handling as well as lots of improvements and bugfixes.
  * Fixes. Ready for testing.
  * Clarification on terminal configurations and key binds.
  * More key options for more terminals.
  * Extended the text area's API to include some useful functions.
  * Setting focus is now based on a "mouse down" event instead of a "click" event.
  * Added Alt-Backspace to text area. Also added a text area demo.
  * Minor clarifications.

  [ Sebastian Rühl ]
  * fix: fixed GetRegionText when colors are active

  [ Oliver ]
  * Only moving the cursor up or down in the text area will preserve the desired column position.
  * Replaced mattn/go-runewidth string width calculation with rivo/uniseg for improved character display.
  * Fixed mouse handling bug for drop-downs. Removed 404s from readme. Fixed #761

 -- Daniel Milde <daniel@milde.cz>  Sun, 02 Oct 2022 21:13:16 +0200

golang-github-rivo-tview (0.0~git20211109.badfa0f-1) unstable; urgency=medium

  * standards bumped to 4.6.0
  * New upstream release

 -- Daniel Milde <daniel@milde.cz>  Sun, 28 Nov 2021 14:57:59 +0100

golang-github-rivo-tview (0.0~git20211001.5508f4b-2) unstable; urgency=medium

  * updated d/gbp.conf

 -- Daniel Milde <daniel@milde.cz>  Mon, 11 Oct 2021 11:50:05 +0200

golang-github-rivo-tview (0.0~git20211001.5508f4b-1) unstable; urgency=medium

  [ Daniel Milde ]
  * add gdu to projects

  [ Oliver ]
  * Removed a project that 404'ed from the README file.
  * Added more clarification for the use of Application.Draw().
  * Upgraded to tcell v2.2.0 and rewrote the Application.Suspend() function to use tcell's new methods.
  * Fixed Box border background colour.
  * Upgraded printing and style handling to the new definition of tcell.ColorDefault.

  [ Daniel P. Berrangé ]
  * Fix inverted handling of KeyPgDn/KeyPgUp in List widget

  [ Quang Ngoc ]
  * Add GoChess to README.md #Projects

  [ raziman ]
  * Add gomu

  [ Oliver ]
  * Cleaned up modules.
  * Upgrade tcell to v2.2.1.
  * Minor clarification.
  * TreeView.process() fires the changed callback and therefore needs to remain in input handler. Fixes #579
  * Fixed missed tag at EOL in TextView. Fixes #531
  * Minor improvements.

  [ darylhjd ]
  * Update README.md

  [ Oliver ]
  * Upgraded to latest tcell version. Fixes #70

  [ Thuc Le ]
  * Add project GoHowMuch into README tview

  [ Oliver ]
  * Fixed wrong mouse click test on drop-downs. Fixes #600
  * Added Application.Sync(), a function to re-sync the screen when its corrupted. Resolves #603
  * Mouse scrolling on TreeView scrolls instead of selecting nodes. Fixes #607
  * Removed an unnecessary call to Focus() in Form. Fixes #612
  * Forms cannot rely purely on a rect check for mouse clicks as drop-downs may extend beyond it. Fixes #602
  * TreeView scrolling now allows current selection to go out of view. Resolves #613

  [ Kanan Rahimov ]
  * Add project DBUI into README tview

  [ Bogdan Naydenov ]
  * update README.md - add ssmbrowse as reference projects using tview

  [ Oliver ]
  * Handling tcell.ErrorEvent now. Fixes #617
  * Added SetListStyles() to DropDown. Resolves #621

  [ Wenshiqi222 ]
  * Update dropdown.go

  [ infl00pLabs ]
  * Add gobit project that uses tview to Readme

  [ Oliver ]
  * Rewrote Table to accommodate virtual tables.
  * Navigation bugfix and improved comments.
  * Upgraded to latest tcell commit. Fixes #624

  [ Takumasa Sakao ]
  * Add viddy: a modern watch command to README

  [ Adrian Moreno ]
  * go.mod: fix wrong reference to tcell dependency

  [ Oliver ]
  * Added a reference go Golang's Code of Conduct instead of rolling our own.
  * Removed accidental inclusion of Postgres driver.
  * Allowing users to determine whether a table selection should wrap. Defaults to false now. Resolves #656
  * Added italics and strikethrough to style attributes. Resolves #457

 -- Daniel Milde <daniel@milde.cz>  Tue, 05 Oct 2021 22:40:52 +0200

golang-github-rivo-tview (0.0~git20210217.8a8f78a-1) experimental; urgency=medium

  [ Michael Vetter ]
  * Fix typo in SetColumns() documentation

  [ Anis uddin Ahmad ]
  * Added geek-life to the list of projects using tview

  [ Stephen Gelman ]
  * Bump standards-version to 4.5.1

  [ Chris Miller ]
  * Update mouse buttons for tcell v2

  [ Oliver ]
  * Allowing list items to shift horizontally. Resolves #512, fixes #513
  * Added SetMaxLines() to TextView. Resolves #451, fixes #452
  * Fixed format for reinserted region tag.
  * Added more bash-like key bindings to InputField. Resolves #549
  * Added RemoveChild() to TreeNode. Resolves #561

 -- Daniel Milde <daniel@milde.cz>  Thu, 04 Mar 2021 23:45:08 +0100

golang-github-rivo-tview (0.0~git20210122.745e4ce-1) unstable; urgency=medium

  [ Daniel Milde]
  * New upstream version 0.0~git20210122.745e4ce

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit.
  * Update standards version to 4.5.1, no changes needed.

 -- Daniel Milde <daniel@milde.cz>  Fri, 22 Jan 2021 15:03:25 +0100

golang-github-rivo-tview (0.0~git20190829.f8bc69b-1) unstable; urgency=medium

  * New upstream version 0.0~git20190829.f8bc69b
    + Add new Depends and B-D: golang-github-rivo-uniseg-dev
  * d/control: Bump the policy std-ver to 4.4.0 (no changes needed)
  * debian: Bump the debhelper version to 12
  * d/gbp.conf: Set explicitly the pristine-tar to False

 -- Jongmin Kim <jmkim@pukyong.ac.kr>  Sat, 31 Aug 2019 23:05:43 +0900

golang-github-rivo-tview (0.0~git20181018.a7c1880-1) unstable; urgency=medium

  [ Jongmin Kim ]
  * Initial release (Closes: #905240)

  [ Felix Lechner ]
  * Set changelog to UNRELEASED
  * Remove Excluded-Files: from d/copyright
  * Set Standards-Version: 4.2.0
  * Provide additional package description
  * Add "in Go" to short description
  * Add upstream's email address to Copyright: in d/copyright
  * Add Upstream-Contact: in d/copyright
  * Fix d/watch to work properly

  [ Jongmin Kim ]
  * Update gbp.conf to use git-pbuilder
  * Set Standards-Version: 4.2.1
  * Reduce leading whitespaces from pkg desc in d/control
  * Remove nickname from Upstream-Contact: in d/copyright
  * Comment the reason to exclude demos in d/rules
  * Set version to 4 in d/watch
  * Configure gbp-clone to auto-run origtargz in d/gbp.conf
  * Set debian-branch to 'debian/sid' in d/gbp.conf
  * Mark UNRELEASED in d/changelog
  * Correct the package version in d/changelog
  * Remove personally configurable keys from d/gbp.conf

 -- Jongmin Kim <jmkim@pukyong.ac.kr>  Thu, 25 Oct 2018 01:24:57 +0900
